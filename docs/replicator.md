# Replicator

Replicator is a bash script that sets up couchdb replication from a source to a target of all databases on a server. The script can be located anywhere that has http/s access to the SERVER where replication is being initiated.


### Purpose

I had a need to move from a bitnami docker based couchdb to stock docker couchdb. I thought this would be as simply as changing the image and changing file ownership as it should be this simple in docker; however, it was not. I do not know the exact issue but I believe the bitnami couchdb instance is setup in some sort of clustered mode which prohibited it from running in a stock couchdb instance.

In any event, I was left with backup/restore, replication, or clustering to make the move. Backup/restore is not as simple as a pg_dumpall + import plus it would led to some (likely minimal) downtime. Clustering my db to move it is too complicated and not needed and Replication seemed to be the perfect fit. The issue was replication occurs at the database level not the server level so you need to setup replication for each database - likely not an issue if you have 2 or 3 databases but it is tedious if you have more.

Enter...replicator! You specify a SERVER, the replication SOURCE and TARGET and it iterates through all databases on the SERVER to set up replication. The data from the SOURCE is usually available on the TARGET by the time the script completes.

There is nothing that ties replicator to converting a bitnami couchdb instance to a stock couchdb instance - it can be used to simply set up a hot-standby instance, backup your data to a different instance or to set up a read replica for your application.


### How it Works

Replicator sets up a replication for each database found on the `SERVER` from the `SOURCE` to `TARGET`.


### How To Set Up

- download the latest [replicator](https://gitlab.com/drad/couchdb-tools/-/raw/master/replicator.sh?inline=false)
    + with curl: `curl -s https://gitlab.com/drad/couchdb-tools/-/raw/master/replicator.sh?inline=false > replicator.sh`
    + we recommend placing the script in a `db/bin` directory in your local project but the location does not matter as long as where the script is being executed has the ability to communicate with the `SERVER` being used.
- make it executable: `chmod u+x replicator.sh`


### How To Use

replicator has several options (see -h for details) but in its most basic usage you will need to supply the replication `SOURCE` and `TARGET` values assuming that the couchdb instance you want to setup replication on is located at `http://admin:couchdb@localhost:5984` (if the couchdb instance is at a different location or you need different credentials you need to set the `SERVER` variable).

- minimal/basic example: `./replicator.sh http://admin:couchdb@db:5984 http://admin:couchdb@db2:5984`
    + this will set up replication from `db` to `db2` on a couchdb instance located at http://admin:couchdb@localhost:5984
- use different couchdb instance: `SERVER=http://admin:admin@my-couchdb-server:5984 ./replicator.sh http://admin:couchdb@db-a:5984 http://admin:couchdb@db-b:5984
    + this will set up replication from `db-a` to `db-b` on a couchdb instance located at http://admin:admin@my-couchdb-server:5984
- basic example but disable continuous replication (e.g. setup one-time-only replication): `CONTINUOUS=false ./replicator.sh http://admin:couchdb@db:5984 http://admin:couchdb@db2:5984`


### How To Migrate a Bitnami CouchDB Instance to Stock CouchDB Instance

I had a bitnami couchdb in docker-compose.yml similar to the following:

```
...
  db:
    # https://hub.docker.com/r/bitnami/couchdb/tags
    image: docker.io/bitnami/couchdb:3.2.2
    environment:
      - COUCHDB_PASSWORD=couchdb
    ports:
      - '5984:5984'
      #- '4369:4369'
      #- '9100:9100'
    volumes:
      # BITNAMI COUCHDB
      # data
      - ./db/data:/bitnami/couchdb
      # config
      - ./db/config/local.d:/opt/bitnami/couchdb/etc/local.d
...
```

Add a stock couchdb instance (notice that we use the same config file):

```
...
  db2:
    restart: no
    # =================================
    # NOTICE: this is a temporary db to allow migration (via replication) from
    #   bitnami couchdb to stock couchdb.
    #   This db is not needed after migration.
    # =================================
    # https://hub.docker.com/_/couchdb/tags
    image: couchdb:3.3.2

    environment:
      - COUCHDB_USER=admin
      - COUCHDB_PASSWORD=couchdb
    ports:
      - '15984:5984'
      #- '4369:4369'
      #- '9100:9100'
    volumes:
      # STOCK COUCHDB
      # config
      - ./config/local.d:/opt/couchdb/etc/local.d
      # data
      - ./db/data2:/opt/couchdb/data
...
```

Now we can run replicator: `./replicator.sh http://admin:couchdb@db:5984 http://admin:couchdb@db2:5984`

Next, we update the db service in docker-compose.yml to use the stock couchdb image and its specific settings as follows:

```
...
  db:
    # https://hub.docker.com/_/couchdb/tags
    image: couchdb:3.3.2
    environment:
      - COUCHDB_USER=admin
      - COUCHDB_PASSWORD=couchdb
    ports:
      - '5984:5984'
      #- '4369:4369'
      #- '9100:9100'
    volumes:
      # STOCK COUCHDB
      # config
      - ./db/config/local.d:/opt/couchdb/etc/local.d
      # data
      - ./db/data2:/opt/couchdb/data
...
```
Now we can stop both databases and bring up the db instance which will be using the stock couchdb image:

```
docker-compose stop db db2 \
    && docker-compose rm -f db db2 \
    && docker-compose up -d db
```

You should now verify that your db (which will be the stock couchdb) no longer has replications as they were on the bitnami instance. Test your application as needed!

