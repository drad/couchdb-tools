#!/bin/bash
# Setup source to target replication for all databases on SERVER.
# This script loops through all databases on SERVER and sets up replication from SOURCE to TARGET.

_version="0.1.4"
_modified="2023-07-06"
_authors="drad"
_created="2023-07-05"
_name="couchdb replicator"
_source="https://gitlab.com/drad/couchdb-tools"


usage="
$(basename "$0") SOURCE TARGET

NOTICE:
    - override the server to get a list of databases from by setting the SERVER variable
        + example: SERVER=http://admin:couchdb@localhost:5984 $(basename "$0") SOURCE TARGET
    - override whether to setup continuous replication by setting the CONTINUOUS variable
        + example: CONTINUOUS=false $(basename "$0") SOURCE TARGET
    - override whether to automatically create the target database by setting the CREATE_TARGET variable
        + example: CREATE_TARGET=false $(basename "$0") SOURCE TARGET

WHERE:
    SOURCE - either local database name or full URL of database (e.g. http://admin:pwd@db1:5984)
    TARGET - either local database name or full URL of database (e.g. http://admin:pwd@db2:5984)
"

if [[ "${1}" == "-v" ]]; then
    echo """
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃  ${_name}  v.${_version} (last modified: ${_modified})                    ┃
┠━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┨
┃    created: ${_created} by ${_authors}                                              ┃
┃    source/docs/issues: ${_source}                ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
"""
    exit 10
fi
if [[ "${1}" == "-h" ]]; then
    echo "${usage}"
    exit 11
fi
if [[ "${1}" == "" ]] || [[ "${2}" == "" ]]; then
    echo "${usage}"
    exit 1
fi

SERVER=${SERVER:-"http://admin:couchdb@localhost:5984"}
CONTINUOUS=${CONTINUOUS:-true}
CREATE_TARGET=${CREATE_TARGET:-true}
#SOURCE=http://admin:couchdb@db:5984
#TARGET=http://admin:couchdb@db2:5984
SOURCE="${1}"
TARGET="${2}"

echo """
REPLICATION SUMMARY
- Server: ${SERVER}
- Repl Source: ${SOURCE}
- Repl Target: ${TARGET}
- Repl Continuous: ${CONTINUOUS}
- Repl Create Target: ${CREATE_TARGET}
---------------------------------
"""

read -p "Do you want to continue? " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Getting all databases..."

    # get all databases
    string=$(http ${SERVER}/_all_dbs | sed 's/\[//' | sed 's/\]//' | sed 's/\"//g')
    IFS=', ' read -a array <<< "$string"

    echo "Looping through databases..."

    # loop through databases and process
    for database in "${array[@]}"; do
        # echo "- db: ${database}"
        if [[ "${database}" == "_global_changes" ]] || [[ "${database}" == "_replicator" ]] || [[ "${database}" == "_users" ]]; then
            echo -e "  - \e[31m✗skipped ${database} (system database)\e[0m"
        else
            echo -n "  - setting up replication for: ${database}  ⟶ "
            resp=$(http -j POST ${SERVER}/_replicator \
                continuous:=${CONTINUOUS} \
                create_target:=${CREATE_TARGET} \
                source="${SOURCE}/${database}" \
                target="${TARGET}/${database}" | jq '.ok')

            if [[ "${resp}" == "true" ]]; then
                echo -e "\e[32mCREATED\e[0m"
            else
                echo -e "\e[31mFAILED\e[0m"
            fi
        fi
    done
    echo -e "\nReplication setup complete!\n"
else
    echo "Replication setup canceled!"
fi
